
# This is used to run the docker container when compiling outside of the docker container
# i.e., run cmake-compile.sh outside of Docker then run this file

# However, for this to work, I'm pretty sure linking libraries has to be changed
# because it currently complains
sudo docker run -ti -p 4280:4280 -v /buildfolderhere/:/buildfolderindocker lithium_docker_image:latest
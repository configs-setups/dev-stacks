#!/bin/bash

# Only run the kill command if the "Kernel" does not have Microsoft in its name.
# This was the easiest way I could think of to check if running under WSL,
# because this kill command doesn't work on WSL:
# "Cannot open /proc/net/unix: No such file or directory"
# dmesg | grep "Microsoft" || kill $(fuser 4280/tcp)

# For some reason, dmesg didn't seem to require sudo on WSL but it does on Arch
kill $(fuser 4280/tcp) || echo "UNIX: Failed to kill server"
# backend-lithium

Lithium C++ backend configuration with a few helpful libraries



## Dependencies



### [RapidJSON](https://rapidjson.org/) (not yet set up)




### [GoogleTest](https://google.github.io/googletest/) (not yet set up)



### Lithium
Use `install-lithium.sh`


You can run the following command
- `> curl https://raw.githubusercontent.com/matt-42/lithium/master/install.sh | bash -s - lithium`

However, this doens't work with directories with spaces in them, so the amended version lives in `./scripts/install-lithium.sh`. You should check the original version in case it gets updated, though.

You will need this to use the `li_symbol_generator`, otherwise you'll have to define symbols manually. Alternatively, you can use the already-existing single-file headers from the GitHub, and look at the source of the `install.sh` file and just run the `g++` command which compiles the `li_symbol_generator`.



### Lithium dependencies
- `> sudo apt install libssl-dev`
- `> sudo apt install libboost-dev`
- `> sudo apt install libboost-all-dev`

https://stackoverflow.com/questions/12578499/how-to-install-boost-on-ubuntu
- `sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev libbz2-dev libboost-all-dev`



#### mysql (my default)
<!-- `> sudo apt install mysql-client` -->
<!-- `> sudo apt install mysql-workbench` (found nothing) -->
<!-- `> sudo apt install libmysqlclient-dev` -->
- `> sudo apt install libmariadbclient-dev`
- `> sudo apt install mariadb-client`



#### sqlite3 (opt)
- `> sudo apt install libsqlite3-dev`



#### curl
- `> sudo apt install libcurl4-openssl-dev`



#### postgresql (opt)
- `> sudo apt install libpq-dev postgresql-server-dev-all`



#### Docker (opt)



## Developing



### Lithium symbols
Provide description/usage here.



### Lithium HTTP Client
Provide description/usage here.



### Lithium HTTP Server
Provide description/usage here.



### Lithium MySQL
Provide description/usage here.



## Building



### CMake
- For any `.cpp` files that use headers, add the `.cpp` file into `add_executable()` in `./CMakeLists.txt`



## Deployment



### CI/CD
Try to set up CI/CD, then provide notes here about it.



### NGINX
This will inherently not be as fast as hitting Lithium straight away, and with testing there was a 3x increase in "requests" per second by omitting NGINX. This was, if I recall correctly, in the tens of thousands even with NGINX on a low-end machine so it probably doesn't matter much.


Route the traffic with proxy_pass

```
## cxx.domain.com
server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	server_name cxx.domain.com;

	location / {
		proxy_pass http://127.0.0.1:4280;
	}
}
```

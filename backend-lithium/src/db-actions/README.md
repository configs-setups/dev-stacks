# db-actions
Write all SQL logic here. SQL stored procedures may be preferable, but even if employing them, all of the database query "interfacing" should be in this folder and grouped by context.
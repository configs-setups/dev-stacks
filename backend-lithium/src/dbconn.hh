#pragma once

#include <lithium_mysql.hh>
#include "conf/conf.hh"


li::mysql_database mysql_db = li::mysql_database(
	s::host = DB_HOST,
	s::database = DB_DATABASE,
	s::user = DB_USER,
	s::password = DB_PASSWORD,
	s::port = DB_PORT,
	s::charset = DB_CHARSET,
	// Only for async connection, specify the maximum number of SQL connections per thread.
	s::max_async_connections_per_thread = DB_MAX_ASYNC_CONNECTIONS_PER_THREAD,
	// Only for synchronous connection, specify the maximum number of SQL connections
	s::max_sync_connections = DB_MAX_SYNC_CONNECTIONS);

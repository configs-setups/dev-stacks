#pragma once

#include "secrets.hh"



// Set the environment
#define ENV_PROD



// -----------------------------------------------------------------------------
// Environment-agnostic configuration
#define YOUR_GENERAL_THINGS "can go here"
// -----------------------------------------------------------------------------



#ifdef ENV_DEVZ
	#define PUBLIC_URL "http://localhost/something"
	#define SERVER_PORT 4280

	#define DB_HOST "127.0.0.1"
	#define DB_DATABASE "database_name"
	#define DB_USER "root"
	#define DB_PASSWORD "password"
	#define DB_PORT 3306
	#define DB_CHARSET "utf8"
	#define DB_MAX_ASYNC_CONNECTIONS_PER_THREAD 200
	#define DB_MAX_SYNC_CONNECTIONS 2000
#endif



#ifdef ENV_PROD
	#define PUBLIC_URL "https://aterlux.com"
	#define SERVER_PORT 4280

	#define DB_HOST "127.0.0.1"
	#define DB_DATABASE "database_name"
	#define DB_USER "very_unique"
	#define DB_PASSWORD "password"
	#define DB_PORT 3306
	#define DB_CHARSET "utf8"
	#define DB_MAX_ASYNC_CONNECTIONS_PER_THREAD 200
	#define DB_MAX_SYNC_CONNECTIONS 2000
#endif

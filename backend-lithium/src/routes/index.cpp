#include <lithium_http_server.hh>
#include <lithium_http_client.hh>

#include "./symbols.hh"

class RouteIndex : public li::http_api
{
public:
	RouteIndex()
	{
		this->get("/test") = [&](li::http_request &request, li::http_response &response)
		{
			response.write("test");
		};

		this->get("/testHeader") = [&](li::http_request &request, li::http_response &response)
		{
			std::cout << "Reqested: /acceptReqSelf" << std::endl;

			response.write(request.header("date"));
		};
	}
};
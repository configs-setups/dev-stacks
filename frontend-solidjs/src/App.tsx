import {Show, Switch, Match, Suspense, createSignal, createComputed, lazy} from "solid-js";
import {Routes, Route, Outlet, RouteDataFunc} from "solid-app-router";
import type {Component, JSXElement} from "solid-js";

import {useStore} from "./store";

import styles from "./App.module.css";
import logo from "./logo.svg";



// -----------------------------------------------------------------------------
// Components
import NavBar from "./components/NavBar";
// -----------------------------------------------------------------------------




// -----------------------------------------------------------------------------
// Pages
import userIdData from "./pages/Users/userId.data";
import Users from "./pages/Users/Users";
import UserLogin from "./pages/Users/UserLogin";
import UserCreate from "./pages/Users/UserCreate";
import UserDetails from "./pages/Users/UserDetails";
import UserPrivileges from "./pages/Users/UserPrivileges";
// -----------------------------------------------------------------------------



/**
 *
 *
 * @return {*}
 */
const App: Component = (): JSXElement => {
	const [store, {pullUser}] = useStore();

	return (
		<div class={styles.appContainer}>
			<NavBar></NavBar>
			<Routes>
				<Route path="/users" element={<Users/>}/>
				<Route path="/users/login" element={<UserLogin/>}/>
				<Route path="/users/create" element={<UserCreate/>}/>
				<Route path="/users/:userId" element={<UserDetails/>} data={userIdData}/>
				<Route path="/users/:userId/">
					<Route path="/privileges" element={<UserPrivileges/>} data={userIdData}/>
				</Route>
			</Routes>
		</div>
	);
};

export default App;

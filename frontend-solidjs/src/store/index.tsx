import {Router} from "solid-app-router";
import {createContext, JSX, useContext} from "solid-js";
import {createStore} from "solid-js/store";

const StoreContext = createContext();
// const RouterContext = createContext();



/**
 * This creates the router and store/state "context" for the app.
 *
 *
 * @export
 * @param {*} props
 * @return {*}
 */
export function Provider(props: any): JSX.Element {
	// Define variables here that will be set as "getters" in the createStore.
	// All of these will be defined using the return of createX
	let articles;

	// -------------------------------------------------------------------------
	// Create the state/store, defining all of the variables and
	// "binding" all of the "created" resources/effects/actions/signals
	// from the separate files
	const [state, setState] = createStore({
		//   get articles() {
		//     return articles();
		//   },
		token: localStorage.getItem("jwt"),
		appName: "frontend-solidjs",
	});
	// -------------------------------------------------------------------------

	const actions = {};
	const store = [state, actions];

	// -------------------------------------------------------------------------
	// "Create" the store actions etc. here
	// articles = createArticles(agent, actions, state, setState);
	// -------------------------------------------------------------------------
	return (
		<Router>
			<StoreContext.Provider value={store}>{props.children}</StoreContext.Provider>
		</Router>
	);
}

/**
 *
 *
 * @return {*}
 */
export function useStore(): any {
	return useContext(StoreContext);
}

import {For} from "solid-js";
import type {JSXElement} from "solid-js";
import {useStore} from "../store";

import styles from "./Column.module.css";



export default (
	props: {
		items: Array<string | JSXElement>
	},
): JSXElement => {
	const [store] = useStore();

	return (
		<div class={styles.tempColumn}>
			<For each={props.items}>{(itm) => {
				return itm;
			}}</For>
		</div>
	);
};

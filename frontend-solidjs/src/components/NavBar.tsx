import {NavLink} from "solid-app-router";
import type {JSXElement} from "solid-js";
import {useStore} from "../store";
import styles from "./NavBar.module.css";



export default (): JSXElement => {
	const [store] = useStore();

	return (
		<div class={styles.navContainer}>
			<h2><NavLink class="txt-light" href="home">{store.appName}</NavLink></h2>
			<NavLink class="txt-light" href="users">users</NavLink>
		</div>
	);
};

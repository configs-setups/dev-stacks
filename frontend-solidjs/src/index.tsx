import {render} from "solid-js/web";
import {Router} from "solid-app-router";
import "./index.css";
import App from "./App";
import {Provider} from "./store";

const rootEl = document.getElementById("root") || document.body;

// rootEl ? render(() => <App />, rootEl) : console.error("Couldn't find root element");


// rootEl ? (
render(() => (
	<Provider>
		<App/>
	</Provider>
), document.body);
// ) :
// console.error("Couldn't find root element");

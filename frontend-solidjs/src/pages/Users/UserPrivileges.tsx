import type {JSXElement} from "solid-js";
import {NavLink, useData} from "solid-app-router";

import {useStore} from "../../store";

import Column from "../../components/Column";



export default (): JSXElement => {
	const [store] = useStore();
	const data = useData();

	return (
		<div>
			<h1>UserPrivileges</h1>
			<div>
				<h2>Info Provided</h2>
				<code>{JSON.stringify(data)}</code>
			</div>
			<div>
				<h2>Actions</h2>
			</div>
		</div>
	);
};

import type {JSXElement} from "solid-js";
import {NavLink} from "solid-app-router";

import {useStore} from "../../store";

import Column from "../../components/Column";



export default (): JSXElement => {
	const [store] = useStore();

	return (
		<div>
			<h1>Users</h1>
			<div>
				<h2>Actions</h2>
				<NavLink href="/users/create">Create user</NavLink>
			</div>
			<div class="column-container">
				<Column items={[
					<h2>User accounts</h2>,
					<h3>Search by username or email</h3>,
					<p>
						avatar (render img),&nbsp;
						username,&nbsp;
						user email,&nbsp;
						is enabled?,&nbsp;
						<NavLink href="/users/1/privileges">[privileges]</NavLink>,&nbsp;
						<NavLink href="/users/1">[user details]</NavLink>,&nbsp;
					</p>,
				]}/>

			</div>
		</div>
	);
};

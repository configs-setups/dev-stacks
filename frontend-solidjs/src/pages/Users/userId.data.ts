import {RouteDataFunc} from "solid-app-router";

export type userIdData = {
	userId: string
}

const userIdData: RouteDataFunc = (props): userIdData => {
	return {
		userId: props.params.userId,
	};
};

export default userIdData;

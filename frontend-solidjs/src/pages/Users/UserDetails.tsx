import type {JSXElement} from "solid-js";
import {NavLink, useData} from "solid-app-router";

import {useStore} from "../../store";

import Column from "../../components/Column";



export default (): JSXElement => {
	const [store] = useStore();
	const data = useData();

	return (
		<div>
			<h1>UserDetails</h1>
			<div>
				<h2>Info Provided</h2>
				<p>List all user details</p>
				<code>{JSON.stringify(data)}</code>
			</div>
			<div>
				<h2>Actions</h2>
				<p>[Enable/Disable user]</p>
				<p>[Enable/Disable 2fa]</p>
				<p>[Link staff]</p>
				<NavLink href="/users/1/privileges">[Edit privileges]</NavLink>
			</div>
		</div>
	);
};

module.exports = {

	// -------------------------------------------------------------------------
	// NOTE: Taken from this uni project of mine:
	// https://gitlab.com/web-tech-degree/ria-gamestart/-/blob/master/gameinc-g/.eslintrc.js
	// -------------------------------------------------------------------------

	"env": {
		"es2020": true,
		// I don't think this should be set to node, but it was complaining about no-undef on the module.exports
		// when this was set to browser
		"node": true,
	},

	// Default standards
	"extends": [
		"eslint:recommended",
		"plugin:solid/recommended",
		"google",
		// Import ruleset
		"./.eslint-global-config.cjs",
	],

	"parserOptions": {
		"ecmaVersion": 2020,
		"sourceType": "module",
		// "tsconfigRootDir": __dirname,
		"project": "./tsconfig.json",
	},

	"plugins": [
		"solid",
	],

	// Global variables so that we don't get screamed at by ESLint
	"globals": {
		// "Atomics": "readonly",
	},


	// -------------------------------------------------------------------------
	// SECTION TypeScript and SolidJS specific
	// -------------------------------------------------------------------------
	"overrides": [{
		// Target only TypeScript files
		"files": [
			"**/*.ts",
			"**/*.tsx",
		],
		"extends": [
			"plugin:@typescript-eslint/eslint-recommended",
			"plugin:@typescript-eslint/recommended",
			// Hallelujah: https://www.npmjs.com/package/eslint-plugin-solid
			"plugin:solid/recommended",
			"google",
			// Import ruleset
			"./.eslint-global-config.cjs",
		],
		"parserOptions": {
			"parser": "@typescript-eslint/parser",
			"ecmaFeatures": {
				"jsx": true,
			},
			"ecmaVersion": 2020,
			"sourceType": "module",
			"project": "./tsconfig.json",
		},
		"plugins": [
			"solid",
			"@typescript-eslint",
		],
		"rules": {
			// Allow the use of ts-ignore for one liners
			// "@typescript-eslint/ban-ts-ignore": "off",
		},
	}],
	// -------------------------------------------------------------------------
	// !SECTION TypeScript specific
	// -------------------------------------------------------------------------

};

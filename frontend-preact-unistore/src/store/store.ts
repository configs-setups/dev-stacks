import createStore, { Store } from "unistore";



export interface UserDetails {
	userId: number,
	privilege: string,
	email: string,
	username: string,
	// By default, API will set displayUsername to the same as username
	displayUsername: string,
	avatatUri: string,
}


export enum NotificationTypes {
	Success,
	Warning,
	Error
}

export interface NotificationParams {
	title: string,
	body: string,
	type: NotificationTypes,
	duration: number
}

// Internally, an ID for each notification is stored,
// but this is not expected as the param for the actions
export interface Notification extends NotificationParams {
	incrementId: number,
}

export interface StoreState {
	count: number,
	authToken: string | undefined,
	// This is an object just so that it's easier, and so that
	// z
	userDetails: UserDetails | undefined,
	notificationIncrementId: number,
	notifications: Notification[]
}

// NOTE: You must manually make this match the actions since I've not figured
// out a way to make the types match because the {x}: StoreState is not assignable
// to these, because then the caller would be expecte to provide that which would be
// wrong
export interface StoreActions {
	increment: (num: number) => void,
	setAuthToken: (str: StoreState) => void,
	setUserDetails: (det: UserDetails) => void,
	addNotification: ( newNotif: NotificationParams) => void,
	removeNotification: (notifIdToRemove: number) => void,
}



// Your initial global app state
export const store = createStore<StoreState>({
	count: 0,
	authToken: undefined,
	userDetails: undefined,
	notificationIncrementId: 0,
	notifications: [],
});



/**
 *
 *
 * @param {*} store
 * @return {*}
 */
export const actions = (store: Store<StoreState>) => ({

	/**
	 *
	 *
	 * @param {StoreState} state
	 * @param {number} inc
	 */
	increment({ count }: StoreState, inc: number) {
		store.setState({ count: count + inc });
	},

	/**
	 *
	 *
	 * @param {StoreState} { authToken }
	 * @param {string} newToken
	 */
	setAuthToken({ authToken }: StoreState, newToken: string) {
		store.setState({ authToken: newToken });
	},

	/**
	 * Set the user's new details
	 *
	 * @param {StoreState} { userDetails }
	 * @param {UserDetails} newUserDetails
	 */
	setUserDetails({ userDetails }: StoreState, newUserDetails: UserDetails) {
		store.setState({ userDetails: newUserDetails });
	},

	/**
	 * Add a notification to the UI notifications display, using a separate ID
	 * in the store to increment the ID of the notification so it can be targeted.
	 * This could probably be replaced with a unix timestamp, but that could be
	 * technically imprecise due to duplicates... not that it would matter.
	 *
	 * @param {StoreState} { notifications }
	 * @param {NotificationParams} newNotif
	 */
	addNotification({ notifications }: StoreState, newNotif: NotificationParams) {
		const currentState = store.getState();
		const notifWithId = <Notification>{
			title: newNotif.title,
			body: newNotif.body,
			type: newNotif.type,
			duration: newNotif.duration,
			incrementId: currentState.notificationIncrementId + 1,
		};
		const newNotifArr = [...notifications];
		newNotifArr.push(notifWithId);
		store.setState({
			notifications: newNotifArr,
			notificationIncrementId: currentState.notificationIncrementId + 1,
		});
	},

	/**
	 * Remove a specific notification from the UI notifications display
	 *
	 * @param {StoreState} { notifications }
	 * @param {number} notifIdToRemove
	 */
	removeNotification({ notifications }: StoreState, notifIdToRemove: number) {
		const currentState = store.getState();
		// FIXME: I always forget filter syntax; this might have the opposite of the desired behaviour
		const notifWithRemoved = currentState.notifications.filter((oldNotif) => {
			return oldNotif.incrementId !== notifIdToRemove;
		});
		store.setState({
			notifications: notifWithRemoved,
		});
	},

});

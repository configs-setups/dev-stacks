declare module "*.css" {
	const mapping: Record<string, string>;
	export default mapping;
}

declare module "*.scss" {
	const mapping: Record<string, string>;
	export default mapping;
}

// declare module "*.scss";

// See: https://wildwolf.name/typescript-preact-and-unistore/

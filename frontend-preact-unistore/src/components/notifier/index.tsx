import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.scss";

// -----------------------------------------------------------------------------
// Store
import { connect } from "unistore/preact";
import { actions, StoreState, StoreActions, store, NotificationParams, NotificationTypes } from "../../store/store";
import { Store } from "unistore";
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// EXPLANATION:
// ActionsPartialStateAndProps inherits the StoreActions, as they shall all be available,
// and whatever you set in MyProps to define this component's props. Then you
// partially inherit parts of the StoreState depending on what you gill to the
// connect() function.
// -----------------------------------------------------------------------------

interface MyProps {
	str: string
}

interface ActionsPartialStateAndProps extends StoreActions, MyProps {
	notifications: StoreState["notifications"]
	// count: StoreState["count"]
}

/**
 * connect's type cast thing is:
 * - connect<T, S, K, I>
 * This, I believe, binds/creates a Preact component constructor with:
 * - T -> nextProps: Readonly<nextProps>
 * - S -> nextState: Readonly<any>
 * - K -> nextContext: any
 *
 * As such, these map as the following:
 * - MyProps -> nextProps
 * - any -> nextState
 * - undefined -> nextContext
 *
 * And I believe ActionsPartialStateAndProps here is essentially just usead "internally"
 * for Unistore.
 *
 * I'm not sure what the second (nextState) should be, but it cannot be undefined.
 *
 * @param {Props} props
 * @return {*}
 */
const Notifier = connect<MyProps, any, undefined, ActionsPartialStateAndProps>("notifications", actions)((props: ActionsPartialStateAndProps) => {
	console.log(props.notifications[0]);
	console.log(props.addNotification);
	console.log(props.str);
	/**
	 *
	 *
	 * @return {*}  {NotificationParams}
	 */
	const notif = (): NotificationParams => {
		return {
			body: "uwu",
			title: (Math.random() * 100).toString(),
			type: NotificationTypes.Success,
			duration: 5000,
		};
	};
	return (
		<div>
			<button onClick={() => props.addNotification(notif())}> add notif</button>
			<p>notifier</p>
			{props.notifications.map((notif, idx) => {
				return (
					<div key={idx}>
						<p>title: {notif.title} __ {notif.body}</p>
						<p>body: {notif.body}</p>
					</div>
				);
			})}
		</div>
	);
});

export default Notifier;

import { FunctionalComponent, h } from "preact";
import { Route, Router } from "preact-router";

// -----------------------------------------------------------------------------
// Store
import { Provider, connect } from "unistore/preact";
import { store, actions, StoreState, StoreActions } from "../store/store";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Pages/Components
import Header from "./header";
import Home from "../routes/home";
import Account from "../routes/account";
import AccountDetails from "../routes/account/details";
import Notfound from "../routes/notfound";
import Notifier from "./notifier";
// -----------------------------------------------------------------------------



/**
 *
 *
 * @return {*}
 */
const App: FunctionalComponent = () => {
	return (
		<Provider store={store}>
			<div id="preact_root">
				<Header />
				<br />
				<br />
				<br />
				<br />
				<Notifier str="asdf" />
				<Router>
					<Route path={`${process.env.PUBLIC_PATH}`} component={Home} />
					<Route path={`${process.env.PUBLIC_PATH}account`} component={Account} />
					<Route path={`${process.env.PUBLIC_PATH}account/:account`} component={AccountDetails} />
					<Notfound default />
				</Router>
			</div>
		</Provider>
	);
};

export default App;

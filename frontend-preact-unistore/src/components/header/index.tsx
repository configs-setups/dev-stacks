import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.scss";

/**
 *
 *
 * @return {*}
 */
const Header: FunctionalComponent = () => {
	return (
		<header class={style.header}>
			<h1>Preact App</h1>
			<nav>
				<Link activeClassName={style.active} href={process.env.PUBLIC_PATH}>Home</Link>
				<Link activeClassName={style.active} href={`${process.env.PUBLIC_PATH }account`}>Account</Link>
			</nav>
		</header>
	);
};

export default Header;

import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import { useEffect } from "preact/hooks";
import style from "./style.scss";

/**
 *
 *
 * @return {*}
 */
const Notfound: FunctionalComponent = () => {
	useEffect(() => {
		console.log(process.env);
		// return () => {
		// 	cleanup;
		// };
	}, []);

	return (
		<div class={style.notfound}>
			<h1>Error 404</h1>
			<p>That page doesn&apos;t exist.</p>
			{/* <Link href={process.env.PUBLIC_PATH}> */}
			<Link href="/">
				<h4>Back to Home</h4>
			</Link>
		</div>
	);
};

export default Notfound;

import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.scss";

/**
 *
 *
 * @param {Props} props
 * @return {*}
 */
const Account: FunctionalComponent = () => {
	return (
		<div class={style.profile}>
			<h1>Account</h1>
		</div>
	);
};

export default Account;

import { FunctionalComponent, h } from "preact";
import style from "./style.scss";

interface Props {
    account: string;
}

/**
 *
 *
 * @param {Props} props
 * @return {*}
 */
const AccountDetails: FunctionalComponent<Props> = (props: Props) => {
	return (
		<div class={style.profile}>
			<h1>AccountDetails</h1>
			<h2>Data</h2>
			<p>Account: {props.account}</p>
		</div>
	);
};

export default AccountDetails;

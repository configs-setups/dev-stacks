import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import { connect } from "unistore/preact";
import style from "./style.scss";

import { actions, StoreState, StoreActions, NotificationParams, NotificationTypes } from "../../store/store";

/**
 *
 *
 * @param {Props} props
 * @return {*}
 */
const Home = connect<StoreState, null, null, StoreActions>(["count"], actions)(
	({ count, increment }) => {
		/**
		 *
		 *
		 * @return {*}  {NotificationParams}
		 */
		const notif = (): NotificationParams => {
			return {
				body: "uwu",
				title: (Math.random() * 100).toString(),
				type: NotificationTypes.Success,
				duration: 5000,
			};
		};
		return (
			<div class={style.home}>
				<h1>Home</h1>
				<p>This is the Home component. Changed to see if CD works (7)</p>
				<p>Count in store: {count}</p>
				<button onClick={() => increment(3)}>increment count</button>
				<h2>Temporary links and things</h2>
				<p><Link activeClassName={style.active} href={`${process.env.PUBLIC_PATH }account/123`}>View account 123</Link></p>
			</div>
		);
	},
);


export default Home;

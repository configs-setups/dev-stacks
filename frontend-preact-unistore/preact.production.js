export default (config, env, helpers) => {
	// -------------------------------------------------------------------------
	// TODO: Improve this file, see: https://github.com/preactjs/preact-cli/blob/master/README.md#custom-configuration
	// -------------------------------------------------------------------------

	// See:
	// - https://stackoverflow.com/a/50076588/13310905
	// - https://github.com/preactjs/preact-cli/issues/463#issuecomment-357760621

	// Apparently this is also required on the VPS, but for some reason this isn't required
	// in local dev
	config.output.publicPath = "/some/sub/directory";

	// Define process.env here
	config.plugins.push(
		new helpers.webpack.DefinePlugin({
			"process.env.PUBLIC_PATH": JSON.stringify("/some/sub/directory"),
			"process.env.API_URL": JSON.stringify("https://yourwebsitedomain.com"),
		}),
	);
};

# frontend



## Developing
`> npm run dev` -> auto-refresh Preact HMR that directly uses the TypeScript source files.
`> npm run serve` -> serve the built files, mostly redundant
`> npm run build-prod` -> build the frontend files with the `./preact.production.js` config
`> npm run build-dev` -> build the frontend files with the `./preact.dev.js` config
`> npm run jest` -> run Jest and Enzyme tests. [*I don't know how this works*](https://github.com/preactjs/enzyme-adapter-preact-pure)
`> npm run eslint` -> run ESLint linting
`> npm run stylelint` -> run Stylelint linting


Also see [Preact CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).



## Unistore
Write here about the bullshit of TypeScript with Unistore. There's a bit of an explainer, and the example of how I've managed to get it to work, in [the notifier component](./src/components/notifier/index.tsx)
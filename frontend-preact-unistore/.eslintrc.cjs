module.exports = {
	env: {
		es2020: true,
		node: true,
	},

	ignorePatterns: [
		"build/",
	],

	// Default standards
	extends: [
		"preact",
		"google",
		// Import ruleset
		"./.eslint-global-config.cjs",
	],

	parserOptions: {
		ecmaVersion: 2020,
		sourceType: "module",
		// "tsconfigRootDir": __dirname,
		project: "./tsconfig.json",
		// ecmaFeatures: {
		//   jsx: true,
		// },
	},

	plugins: [
	],

	// Global variables so that we don't get screamed at by ESLint
	globals: {
		// "Atomics": "readonly",
	},


	// -------------------------------------------------------------------------
	// SECTION TypeScript specific
	// -------------------------------------------------------------------------
	overrides: [{
		// Target only TypeScript files
		files: [
			"**/*.ts",
			"**/*.tsx",
		],
		extends: [
			"preact",
			"plugin:@typescript-eslint/eslint-recommended",
			"plugin:@typescript-eslint/recommended",
			"google",
			// Import ruleset
			"./.eslint-global-config.cjs",
		],
		parserOptions: {
			project: "./tsconfig.json",
			parser: "@typescript-eslint/parser",
		},
		plugins: [
			"@typescript-eslint",
		],
		rules: {
			// Allow the use of ts-ignore for one liners
			// "@typescript-eslint/ban-ts-ignore": "off",
		},
	}],
	// -------------------------------------------------------------------------
	// !SECTION TypeScript specific
	// -------------------------------------------------------------------------

};

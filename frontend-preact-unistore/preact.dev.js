export default (config, env, helpers) => {
	// -------------------------------------------------------------------------
	// TODO: Improve this file, see: https://github.com/preactjs/preact-cli/blob/master/README.md#custom-configuration
	// -------------------------------------------------------------------------

	// See:
	// - https://stackoverflow.com/a/50076588/13310905
	// - https://github.com/preactjs/preact-cli/issues/463#issuecomment-357760621

	// Define process.env here
	config.plugins.push(
		new helpers.webpack.DefinePlugin({
			"process.env.PUBLIC_PATH": JSON.stringify("/"),
			"process.env.API_URL": JSON.stringify("http://localhost:3011/"),
		}),
	);
	// const sass = helpers.getLoadersByName(config, "sass-loader")[0];
	// sass.loader.options.sourceMap = true;
};
